# Dossiers
SRC_DIR = src
BIN_DIR = bin

# Fichiers sources
SRCS = $(wildcard $(SRC_DIR)/*.c)

# Fichiers objets
OBJS = $(patsubst $(SRC_DIR)/%.c,$(BIN_DIR)/%.o,$(SRCS))

# Nom de l'exécutable
EXECUTABLE = fp4

# Compilation de l'exécutable
all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJS)
	gcc -o $(EXECUTABLE) $(OBJS) -lSDL2main -lSDL2 -g

# Règle générale pour la création des fichiers objets
$(BIN_DIR)/%.o: $(SRC_DIR)/%.c
	gcc -g -c $< -o $@

# Le menage
clean:
	rm -f $(BIN_DIR)/*.o $(EXECUTABLE)
